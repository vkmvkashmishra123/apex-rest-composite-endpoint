# Apex REST Composite Endpoint

Below is Salesforce developer documentation for Composite REST endpoint: 
https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/resources_composite_composite.htm

We want to replicate exactly the same via a custom Apex REST Service. Above document is having all good required pointers to request and response body, along with example snippets. For quick reference here are some of those articles
Request Structure
Response Structure
Example: Execute Dependent Requests in a Single API Call
Example: Update an Account, Create a Contact, and Link Them with a Junction Object