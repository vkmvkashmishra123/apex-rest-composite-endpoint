/*
Apex Class: CustomCompositeRestServiceFalse
Description: This class is used to performs all DML operations for request POST, GET, PATCH, DELETE when the value of allOrNone  is false.
Developer Name: Vikash Kumar Mishra
*/
global class CustomCompositeRestServiceFalse { 
    // crete dynamic SObject.
    public static SObject createDynamicsObj; 
    // to check how many success records we have.
    public static Integer globalVar = -1;	
    // contain all records referenceId with values one by one when it would be success.
    public static Map<String, Object> createdRecords = new Map<String, Object>();	
    // it contains body of records.
    public static  Set<String> setOfBodyKey = new Set<String>();	
    // it contains response according to request.
    public static List<CustomCompositeRestResponseHandler> listOfResponse = new List<CustomCompositeRestResponseHandler>();	
    
    /*
@method getRequest: it called from the CustomCompositeRestRequest to get request into string. 
@param json: it contains json string.
@return : response type of List<CustomCompositeRestResponseHandler>.
*/
    public static List<CustomCompositeRestResponseHandler>getRequest(String json) {
        //@records: it would contain all requested records in primitive type.
        Map<String, Object> records = (Map<String, Object>)System.JSON.deserializeUntyped(json);
        Object obj = records.get('compositeRequest');
        String str = System.JSON.serialize(obj);
        List<Object> rawObj = (List<Object>) System.JSON.deserializeUntyped(str);
        //@savepoint sp: Basically is used to rollback if occure any exception. 
        try{
            getRecords(rawObj);
        }catch(Exception e){
            System.debug(globalVar);
            System.debug(listOfResponse.size());
            //@m:it is basically is used to check where we got exceptions. 
            Integer m=0;
            for(Object ob : rawObj) {
                if(m<=globalVar){
                    m++;
                    system.debug(' if m: '+m);
                }
                else{
                    system.debug('else m: '+m);
                    String strng = System.JSON.serialize(ob);
                    Map<String, Object> compreq = (Map<String,Object>) System.JSON.deserializeUntyped(strng);
                    String refId = (String)compreq.get('referenceId');
                    //creating object of CustomCompositeRestResponseHandler class to generate response
                    CustomCompositeRestResponseHandler response = new CustomCompositeRestResponseHandler();
                    CustomCompositeRestResponseHandler.Body respBody=new CustomCompositeRestResponseHandler.Body();
                    CustomCompositeRestResponseHandler.Headers respHeaders = new CustomCompositeRestResponseHandler.Headers();
                    CustomCompositeRestResponseHandler.Errors respErrors = new CustomCompositeRestResponseHandler.Errors();
                    
                    respBody.success = 'false';
                    respErrors.errorCode='Process_Halted';
                    respErrors.message ='Execution halted as previous operation was not successful';
                    respBody.errors = respErrors;
                    response.responseBody = respBody;
                    response.referenceId = refId;
                    
                    listOfResponse.add(response);
                    m++; 
                }
            }
        }
        return listOfResponse;
    } 
    
    /*
@method postMethod: this method is used to create records.
@param recordOfMap: it is type of Map<String, Object> and contain records which is being created. 
*/
    public static void postMethod( Map<String, Object> mp) {
        List<String> listofKeyandField = new List<String>();
        //creating object of CustomCompositeRestResponseHandler class to generate response
        CustomCompositeRestResponseHandler response = new CustomCompositeRestResponseHandler();
        CustomCompositeRestResponseHandler.Body respBody=new CustomCompositeRestResponseHandler.Body();
        CustomCompositeRestResponseHandler.Headers respHeaders = new CustomCompositeRestResponseHandler.Headers();
        CustomCompositeRestResponseHandler.Errors respErrors = new CustomCompositeRestResponseHandler.Errors();
        //@flag: it is used to check the object is exist or not in salesforce if object exist then flag should not be changed and if not flag would be one. 
        Integer flag = 0;
        String url1 = (String)mp.get('url');
        String refId = (String)mp.get('referenceId');
        String objectName = url1.substringAfterLast('/');
        //@allSObjects: contains all standard SObject
        Map<String, Schema.SObjectType> allSObjects = Schema.getGlobalDescribe();
        if(allSObjects.containsKey(objectName)){
            flag = 1;
        }
        if(flag == 0){    
            respBody.success = 'false';
            respErrors.errorCode='Process_Halted';
            respErrors.message ='The Object name does not exit '+objectName;
            respBody.errors = respErrors;
            response.responseBody = respBody;
            response.referenceId = refId;
            listOfResponse.add(response); 
        }
        else{
            Object ob2 =mp.get('body');
            Map<String, Object> mapofbody = (Map<String, Object>)ob2;
            Set<String> listOfFields =new Set<String>();
            SObjectType accountType = Schema.getGlobalDescribe().get(objectName);
            Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
            for (String mfield : mfields.keySet()){
                listOfFields.add(mfield);   
            }
            if(setOfBodyKey.size() !=0){
                setOfBodyKey= new Set<String>();
            }
            for(String field : mapofbody.keySet()){
                String str = field.toLowerCase();
                setOfBodyKey.add(str);            
            }  
            createDynamicsObj = Schema.getGlobalDescribe().get(objectName).newSObject();
            try{
                for (String fieldName : mapofbody.keySet()){
                    if(((String)mapofbody.get(fieldName)).contains('@{')){
                        String keyValueOfBody = ((String)mapofbody.get(fieldName)).substringBetween('{', '}');
                        listofKeyandField = keyValueOfBody.split('\\.');
                        Object ob = createdRecords.get(listofKeyandField[0]);
                        String jsonserili = System.JSON.serialize(ob);
                        Map<String, Object> ob1= (Map<String, Object>)System.JSON.deserializeUntyped(jsonserili);
                        String val = (String)ob1.get(listofKeyandField[1]);
                        
                        if(listofKeyandField[1]=='Id' || listofKeyandField[1]=='ID' || listofKeyandField[1]=='id') {
                            val = (String)ob1.get('Id');
                        }
                        
                        String OriginalValue = (String)mapofbody.get(fieldName);
                        OriginalValue = OriginalValue.remove('@{'+keyValueOfBody+'}');
                        OriginalValue = OriginalValue+val;
                        
                        mapofbody.put(fieldName, OriginalValue);         
                    } else{
                        createDynamicsObj.put(fieldName, mapofbody.get(fieldName));
                    }
                }
                for(String ssss : mapofbody.keySet()){
                    createDynamicsObj.put(ssss, mapofbody.get(ssss));       
                }
                
                insert createDynamicsObj;
                createdRecords.put(refId, createDynamicsObj);
                respBody.id = (String)createDynamicsObj.get('Id');
                respBody.success = 'true';                 
                respHeaders.location =url1+'/'+(String)createDynamicsObj.get('Id');
                response.responseBody = respBody;
                response.headers = respHeaders;
                response.referenceId = refId; 
                listOfResponse.add(response);
            }catch(System.DMLException e){   
                respBody.success = 'false';
                respErrors.errorCode=e.getDmlStatusCode(0);//'Process_Halted';
                respErrors.message = e.getDMLMessage(0);
                if(listofKeyandField.size() != 0){
                    if(createdRecords.containsKey(listofKeyandField[0])){
                        respErrors.message =e.getMessage();
                    }else{
                        respErrors.message = 'Could not find the referenced operation '+listofKeyandField[0];
                    } 
                }   
                respBody.errors = respErrors;
                response.responseBody = respBody;
                response.referenceId = refId;
                listOfResponse.add(response);
            }catch(Exception e) {
                 respBody.success = 'false';
                respErrors.errorCode= 'Process_Halted';
                respErrors.message = e.getMessage();
                if(listofKeyandField.size() != 0){
                    if(createdRecords.containsKey(listofKeyandField[0])){
                        respErrors.message =e.getMessage();
                    }else{
                        respErrors.message = 'Could not find the referenced operation '+listofKeyandField[0];
                    } 
                }   
                respBody.errors = respErrors;
                response.responseBody = respBody;
                response.referenceId = refId;
                listOfResponse.add(response);
            }
            //createdRecords.put(refId, createDynamicsObj);
        }
    } 
    
    
    /*
@method getMethod: this method is used to get records with matching referenceId.
@param recordOfMap: it is type of Map<String, Object> and contain records which is being updated. 
*/
    public static void getMethod( Map<String, Object> recordOfMap) {
        String jsonString;
        List<String> listOfValue = new List<String>();
        //creating object of CustomCompositeRestResponseHandler class to generate response
        CustomCompositeRestResponseHandler response = new CustomCompositeRestResponseHandler();
        CustomCompositeRestResponseHandler.Body respBody=new CustomCompositeRestResponseHandler.Body();
        CustomCompositeRestResponseHandler.Headers respHeaders = new CustomCompositeRestResponseHandler.Headers(); 
        CustomCompositeRestResponseHandler.Errors respErrors = new CustomCompositeRestResponseHandler.Errors();
        
        String url1 = (String)recordOfMap.get('url');
        System.debug(url1);
        
        String refId = (String)recordOfMap.get('referenceId');
        System.debug(refId);
        
        String urlg = url1.substringBeforeLast('/');
        String objString =  urlg.substringAfterLast('/');
        
        String getId = url1.substringAfterLast('/');
        String getId1 = getId;
        //@flag: it is used to check the object is exist or not in salesforce if object exist then flag should not be changed and if not flag would be one.
        Integer flag = 0;
        
        //@allSObjects: contains all standard SObject
        Map<String, Schema.SObjectType> allSObjects = Schema.getGlobalDescribe();
        if(allSObjects.containsKey(objString)){
            flag = 1;
            System.debug(flag);
        }
        if(flag == 0){  
            respBody.success = 'false';
            respErrors.errorCode='Process_Halted';
            respErrors.message ='The Object name does not exit '+objString;
            respBody.errors = respErrors;
            response.responseBody = respBody;
            response.referenceId = refId;
            listOfResponse.add(response); 
        }
        else{
            if(getId.contains('@{')){
                getId = getId.substringBetween('{', '}');  
                System.debug(getId);
                listOfValue = getId.split('\\.') ;
            
                Object ob = createdRecords.get(listOfValue[0]);
                //@jsonserpretty: it holds Json String
                String jsonserpretty = System.JSON.serializePretty(ob);
                try{
                    Map<String, Object> ob1= (Map<String, Object>)System.JSON.deserializeUntyped(jsonserpretty);
                    if(listOfValue[1]=='Id' || listOfValue[1]=='ID' || listOfValue[1]=='id'){
                        getId = (String)ob1.get('Id');
                        List<String> jsonStrg = new List<String>();
                        jsonStrg.add(jsonserpretty);
                        respBody.records = jsonStrg;
                        respBody.success = 'true';
                        respBody.id = getId;
                        response.responseBody = respBody;
                        response.referenceId = refId;
                        listOfResponse.add(response);
                        createdRecords.put(refId, ob1);
                    }
                    
                    else if(listOfValue[1]=='OwnerId' || listOfValue[1]=='ownerId' || listOfValue[1]=='ownerid' || listOfValue[1]=='OWNERID' ) {  
                        String fieldss =  getId1.substringAfterLast('=');
                        List<String> listOfFields = fieldss.split('\\,');
                        String query2 = 'Id';
                        for(String s : listOfFields){
                            query2 = query2+' , '+s;
                        }
                        getId = (String)ob1.get('Id');
                        String sObjName = Id.valueOf(getId).getSObjectType().getDescribe().getName();
                        String query1 = 'select '+ listOfValue[1]+' from '+ sObjName+  ' where Id=\''+getId+'\'';
                        SObject sob =  Database.query(query1);                
                        String ownId = (String)sob.get('OwnerId');
                        query2 = 'SELECT '+query2+' from '+ objString+ ' where  Id=\''+ownId+'\'';
                        sob =  Database.query(query2);
                        jsonString=System.JSON.serializePretty(sob);
                        
                        List<String> listOfRecords = new List<String>();
                        listOfRecords.add(jsonString);
                        
                        respBody.records = listOfRecords;
                        respBody.success = 'true';
                        respBody.id = getId;
                        response.responseBody = respBody;
                        response.referenceId = refId;
                        listOfResponse.add(response);
                        createdRecords.put(refId, sob);
                    }else{
                        respBody.success = 'false';
                        respErrors.errorCode='NOT_FOUND';
                        respErrors.message = 'Provided external ID field does not exist or is not accessible: '+getId;
                        respBody.errors = respErrors;
                        response.responseBody = respBody;
                        response.referenceId = refId;
                        listOfResponse.add(response);
                    }
                }catch(System.DMLException ex){
                    respBody.success = 'false';
                    respErrors.errorCode=ex.getDmlStatusCode(0);//'Process_Halted';
                    if(listOfValue.size() == 1)
                        respErrors.message = '"'+ getId+'"' + ' references an invalid Datatype. Only Strings and primitive data types are allowed to be referenced from previous operations.';
                    else
                        respErrors.message ='Invalid reference specified. No value for '+getId+' found in '+ listOfValue[0];
                    respBody.errors = respErrors;
                    response.responseBody = respBody;
                    response.referenceId = refId;
                    listOfResponse.add(response);   
                }catch(Exception ex) {
                    respBody.success = 'false';
                    respErrors.errorCode='Procees_Halted';
                    respErrors.message = 'Could not find the referenced operation ' +getId+ ' OR Execution halted as previous operation was not successful';
                    respBody.errors = respErrors;
                    response.responseBody = respBody;
                    response.referenceId = refId;
                    listOfResponse.add(response);  
                }
            }
            else{
                respBody.success = 'false';
                respErrors.errorCode='NOT_FOUND';
                respErrors.message = 'Provided external ID field does not exist or is not accessible: '+getId;
                respBody.errors = respErrors;
                response.responseBody = respBody;
                response.referenceId = refId;
                listOfResponse.add(response);   
            }
        }
    } 
    
    
      /*
   @method patchMethod: basically this method will update records with matching referenceId.
   @param recordOfMap: it is type of Map<String, Object> and contain records which is being updated. 
   */
    public static void patchMethod( Map<String, Object> recordOfMap) { 
        String url1 = (String)recordOfMap.get('url');        
        String refId = (String)recordOfMap.get('referenceId');
        
        String urlg = url1.substringBeforeLast('/');
        String objectName =  urlg.substringAfterLast('/');
        
        String getId = url1.substringAfterLast('/');
        //@flag: it is used to check the object is exist or not in salesforce if object exist then flag should not be changed and if not flag would be one.
        Integer flag = 0;
        //creating object of CustomCompositeRestResponseHandler class to generate response
        CustomCompositeRestResponseHandler response = new CustomCompositeRestResponseHandler();
        CustomCompositeRestResponseHandler.Body respBody=new CustomCompositeRestResponseHandler.Body();
        CustomCompositeRestResponseHandler.Headers respHeaders = new CustomCompositeRestResponseHandler.Headers();
        CustomCompositeRestResponseHandler.Errors respErrors = new CustomCompositeRestResponseHandler.Errors();
        
        Map<String, Schema.SObjectType> allSObjects = Schema.getGlobalDescribe();
        if(allSObjects.containsKey(objectName)){
            flag = 1;
        }
        if(flag == 0){
            respBody.success = 'false';
            respErrors.errorCode='Process_Halted';
            respErrors.message ='The Object name does not exit '+objectName;
            respBody.errors = respErrors;
            response.responseBody = respBody;
            response.referenceId = refId;
            listOfResponse.add(response); 
        }
        else{
            Object getBody =recordOfMap.get('body');
            Map<String, Object> mapofbody = (Map<String, Object>)getBody;
            
            createDynamicsObj = Schema.getGlobalDescribe().get(objectName).newSObject();
            try{
                for (String fieldName : mapofbody.keySet()) { 
                    if(((String)mapofbody.get(fieldName)).contains('@{')){
                        String keyValueOfBody = ((String)mapofbody.get(fieldName)).substringBetween('{', '}');
                        List<String> listofKeyandField = keyValueOfBody.split('\\.');
                        Object ob = createdRecords.get(listofKeyandField[0]);
                        
                        String jsonserili = System.JSON.serialize(ob);
                        Map<String, Object> obj= (Map<String, Object>)System.JSON.deserializeUntyped(jsonserili);
                        String value = (String)obj.get(listofKeyandField[1]);
                        
                        String OriginalValue = (String)mapofbody.get(fieldName);
                        OriginalValue = OriginalValue.remove('@{'+keyValueOfBody+'}');
                        OriginalValue = OriginalValue+value;
                        
                        mapofbody.put(fieldName, OriginalValue);         
                    }
                    else{
                        createDynamicsObj.put(fieldName, mapofbody.get(fieldName));
                    }
                    System.debug(createDynamicsObj);
                }   
                for(String ssss : mapofbody.keySet()){
                    createDynamicsObj.put(ssss, mapofbody.get(ssss));       
                }
                createDynamicsObj.put('Id', getId);
                update createDynamicsObj;
                createdRecords.put(refId, createDynamicsObj);
                respBody.id = (String)createDynamicsObj.get('Id');
                respBody.success = 'true';                 
                respHeaders.location =url1+'/'+(String)createDynamicsObj.get('Id');
                response.responseBody = respBody;
                response.headers = respHeaders;
                response.referenceId = refId; 
                listOfResponse.add(response);
            }catch(System.DMLException e){
                respBody.success = 'false';
                respErrors.errorCode= e.getDmlStatusCode(0);//'Process_Halted';
                respErrors.message = e.getDMLMessage(0);
                respBody.errors = respErrors;
                response.responseBody = respBody;
                response.referenceId = refId;
                listOfResponse.add(response);                    
            }catch(Exception e){
                respBody.success = 'false';
                respErrors.errorCode= 'Process_Halted';
                respErrors.message = e.getMessage();
                respBody.errors = respErrors;
                response.responseBody = respBody;
                response.referenceId = refId;
                listOfResponse.add(response);                   
            } 
           // createdRecords.put(refId, createDynamicsObj);
        }      
    }
    
        /*
   @method deleteMethod: basically this method will delete records with matching referenceId.
   @param recordOfMap: it is type of Map<String, Object> and contain records which is being deleted. 
   */
    public static void deleteMethod( Map<String, Object> recordOfMap) {
        //@url1: hold url
        String url1 = (String)recordOfMap.get('url');        
        String refId = (String)recordOfMap.get('referenceId');
        
        String urlg = url1.substringBeforeLast('/');
        //@objectName: contain object which is comming from url
        String objectName =  urlg.substringAfterLast('/');
        
        String getId = url1.substringAfterLast('/');
        //@flag: it is used to check the object is exist or not in salesforce if object exist then flag should not be changed and if not flag would be one.
        Integer flag = 0;
        //creating object of CustomCompositeRestResponseHandler class to generate response
        CustomCompositeRestResponseHandler response = new CustomCompositeRestResponseHandler();
        CustomCompositeRestResponseHandler.Body respBody=new CustomCompositeRestResponseHandler.Body();
        CustomCompositeRestResponseHandler.Headers respHeaders = new CustomCompositeRestResponseHandler.Headers();
        CustomCompositeRestResponseHandler.Errors respErrors = new CustomCompositeRestResponseHandler.Errors();
        //@allSObjects: it contains all standard Sobjects.
        Map<String, Schema.SObjectType> allSObjects = Schema.getGlobalDescribe();
        if(allSObjects.containsKey(objectName)){
            flag = 1;
        }
        if(flag == 0){
            respBody.success = 'false';
            respErrors.errorCode='Process_Halted';
            respErrors.message ='The Object name does not exit'+objectName;
            respBody.errors = respErrors;
            response.responseBody = respBody;
            response.referenceId = refId;
            listOfResponse.add(response); 
        }     
        createDynamicsObj = Schema.getGlobalDescribe().get(objectName).newSObject();
        createDynamicsObj.put('id', getId );
        try{
            delete createDynamicsObj;
            createdRecords.put(refId, createDynamicsObj);
        }catch(System.DMLException ex){
            respBody.success = 'false';
            respErrors.errorCode=ex.getDmlStatusCode(0);
            respErrors.message =ex.getDMLMessage(0);
            respBody.errors = respErrors;
            respBody.id = getId;
            response.responseBody = respBody;
            response.referenceId = refId;
            listOfResponse.add(response); 
        }catch(Exception e){
            respBody.success = 'false';
            respErrors.errorCode= 'Process_Halted';
            respErrors.message = e.getMessage();
            respBody.errors = respErrors;
            response.responseBody = respBody;
            response.referenceId = refId;
            listOfResponse.add(response);                   
        } 
        //createdRecords.put(refId, createDynamicsObj);
    }
    
    
    /*
@method getRecords: get records one by one from List<Object> and find which method request have come. After finding method, it would call matching method.
@param rawObj: it contains all request records in primitive type. 
*/
    public static void getRecords(List<Object> rawObj){
        String jsonString = null;
        for(Object ob : rawObj) {
            String strg = System.JSON.serialize(ob);
            Map<String, Object> compreq = (Map<String,Object>) System.JSON.deserializeUntyped(strg);
            if(compreq.get('method')=='POST'){
                globalVar++;
                postMethod(compreq);
            }
            else if(compreq.get('method')=='GET'){
                globalVar++;
                getMethod(compreq);
            } 
            else if(compreq.get('method')=='PATCH'){
                globalVar++;
                patchMethod(compreq);
            }
            else if(compreq.get('method')=='DELETE'){
                globalVar++;
                deleteMethod(compreq);
            }
            else{
                 globalVar++;
                //creating object of CustomCompositeRestResponseHandler class to generate response
                CustomCompositeRestResponseHandler response = new CustomCompositeRestResponseHandler();
                CustomCompositeRestResponseHandler.Body respBody=new CustomCompositeRestResponseHandler.Body();
                CustomCompositeRestResponseHandler.Headers respHeaders = new CustomCompositeRestResponseHandler.Headers();
                CustomCompositeRestResponseHandler.Errors respErrors = new CustomCompositeRestResponseHandler.Errors();
                respBody.success = 'false';
                respErrors.errorCode='Process_Halted';
                respErrors.message ='Unsupported HTTP method '+compreq.get('method') ;
                respBody.errors = respErrors;
                //respBody.id = getId;
                response.responseBody = respBody;
                response.referenceId = (String)compreq.get('referenceId');
                listOfResponse.add(response); 
            }
        } 
    } 
}