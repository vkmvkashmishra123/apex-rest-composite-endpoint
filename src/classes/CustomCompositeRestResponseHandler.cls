/*
Apex Class: CustomCompositeRestResponseHandler
Description: This class is used to generate response of custom composite request.
Developer Name: Vikash Kumar Mishra
*/
global class CustomCompositeRestResponseHandler {
    public Body responseBody {get; set;}
    public Headers headers {get; set;}
    public String referenceId {get; set;}
    /*
	*Apex Class: Body
	*Description: This class is used to get response body like we do get request, after success request records would be in Body class.
	*/
    public class Body {
        public String id {get; set;}
        public String success {get; set;}
        public List<String> records {get; set;}
        public Errors errors {get; set;}      
    }
    /*
	* Apex Class: Headers
	* Description: This class is used to get location like location: /services/data/v38.0/sobjects/Account/0017F00000tTK6cQAG.
	*/
    public class Headers {
        public string location {get; set;}
    }
    /*
	* Apex Class: Errors
	* Description: This class is used to get errors.
	*/
    public class Errors{
        public string referenceId {get; set;}
        public string errorCode {get; set;}
        public string message {get; set;}
    }
}