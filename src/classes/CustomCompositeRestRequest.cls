/*
Apex Class: CustomCompositeRestRequest
Description: This class is used to accept composite request in json format and after acceptng request, will call CustomCompositeRestService to perform all operations like Post, Get, etc.
Developer Name: Vikash Kumar Mishra
*/
@RestResource(urlMapping = '/compositeRequest/*')
global class CustomCompositeRestRequest {
    @HttpPost
    global static List<CustomCompositeRestResponseHandler> doPost() {
        RestResponse restResponse = RestContext.response;
        RestRequest restRequest = RestContext.request;
        try{
            String jstring = restRequest.requestBody.toString();
            Map<String, Object> records = (Map<String, Object>)System.JSON.deserializeUntyped(jstring);
            //@flag: it is used to hold the value of allOrNone
            String flag = String.valueOf(records.get('allOrNone'));
            //@response: it will contain all response which is return by getRequest Method of CustomCompositeRestService class
            if(flag == 'true'){
                List<CustomCompositeRestResponseHandler> response = CustomCompositeRestService.getRequest(jstring);
                return response;
            }
            else{
                List<CustomCompositeRestResponseHandler> response = CustomCompositeRestServiceFalse.getRequest(jstring);
                return response;
            }
        }catch(System.JSONException ex){
            List<CustomCompositeRestResponseHandler> response = new  List<CustomCompositeRestResponseHandler>();
            
            CustomCompositeRestResponseHandler responseHandle = new CustomCompositeRestResponseHandler();
            CustomCompositeRestResponseHandler.Body respBody=new CustomCompositeRestResponseHandler.Body();
            CustomCompositeRestResponseHandler.Headers respHeaders = new CustomCompositeRestResponseHandler.Headers();
            CustomCompositeRestResponseHandler.Errors respErrors = new CustomCompositeRestResponseHandler.Errors();
            
            respErrors.errorCode=ex.getTypeName();
            respErrors.message = ex.getMessage()+'\n'+ex.getStackTraceString();
            respBody.errors = respErrors;
            responseHandle.responseBody = respBody;
            response.add(responseHandle);
            return response;
        }
        
    }
}